const { client, helper } = require('@nioh/pack')

helper
  .set('baseDir', __dirname)
  .set('preset', 'vue')
  .set('entryInKoa', ['home', 'login'])

module.exports = client
