import Vue from 'vue'
import App from './page/Login.vue'
import onevue from 'one-vue'
import './style/base.css'
import './style/layout.less'

Vue.use(onevue)
Vue.config.devtools = true

const app = new Vue({
  render: h => h(App)
})

app.$mount('#app')
