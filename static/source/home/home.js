import Vue from 'vue'
import App from './page/Home.vue'
import onevue from 'one-vue'

Vue.use(onevue)
Vue.config.devtools = true

const app = new Vue({
  render: h => h(App)
})

app.$mount('#app')
