import Vue from 'vue'
import App from './pages/App.vue'

// import onevue from '@ui/one-vue'
import store from '@/store'
import router from '@/router'
import '@/style/base.less'
import '@/style/app.less'

// Do not delete: dynamic publicPath for lazy import modules
__webpack_public_path__ = window.RES_PUBLIC_PATH // eslint-disable-line

Vue.config.productionTip = false
Vue.config.devtools = true

// Vue.use(onevue)
const el = '#app'
const app = new Vue({
  router,
  store,
  render: h => h(App)
})

app.$mount(el)
