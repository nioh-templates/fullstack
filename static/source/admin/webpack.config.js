const { client, helper } = require('@nioh/pack')
const { merge, resolve } = helper

helper
  .set('baseDir', __dirname)
  .set('preset', 'vue')
  .set('patch', (rules) => {
  })
// 基于 @server/core 项目使用以下方法配置入口，并移除 merge 中的配置
helper.set('entryInKoa', ['app'])

// merge webpack config
module.exports = merge(client, {
  resolve: {
    alias: {
      '@': resolve(__dirname, './')
    }
  }
})
