export default {
  // static options
  menu: [
    {
      name: '首页',
      icon: 'house',
      link: '/'
    },
    {
      name: '文章列表',
      icon: 'document',
      link: '/articles'
    },
    {
      name: '用户',
      icon: 'user',
      link: '/users'
    },
    {
      name: '角色管理',
      icon: 'files',
      link: '/roleactions'
    }
  ],
  // user data
  user: {
    //
  }
}
