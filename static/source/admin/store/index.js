import Vue from 'vue'
import Vuex from 'vuex'

// global state
import state from './state'
import actions from './action'
import mutations from './mutation'
import getters from './getter'

// modules

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
  },
  state,
  actions,
  mutations,
  getters
})

export default store
