import axios from 'axios'

const api = axios.create({
  timeout: 1000 * 10
})

api.interceptors.response.use(res => {
  if (res.status === 200) {
    return Promise.resolve(res.data)
  } else {
    return Promise.reject(res.data)
  }
}, err => {
  return Promise.reject(err)
})

export const getEvent = async (id) => {
  const opt = {
    url: '/api/events',
    params: {
      id,
      v: Math.random()
    },
    method: 'GET'
  }
  const res = await api.request(opt)
  return res
}

export const updateEvent = async (id, data) => {
  const opt = {
    method: 'PUT',
    url: '/api/event',
    data: data,
    params: {
      id,
      type: 'detail',
      v: Math.random()
    }
  }

  const res = await api.request(opt)
  return res
}

export const getComponents = async (query) => {
  const opt = {
    url: '/api/components',
    params: {
      ...query,
      v: Math.random()
    },
    method: 'GET'
  }
  const res = await api.request(opt)
  return res
}

export const createComponent = async (data) => {
  const opt = {
    method: 'POST',
    url: '/api/component',
    data: data,
    params: {
      v: Math.random()
    }
  }

  const res = await api.request(opt)
  return res
}

export const updateComponent = async (data) => {
  const opt = {
    method: 'PUT',
    url: '/api/component',
    data: data,
    params: {
      v: Math.random()
    }
  }
  const res = await api.request(opt)
  return res
}
