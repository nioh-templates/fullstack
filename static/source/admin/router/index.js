import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const config = {
  mode: 'history',
  base: '/admin',
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        breadcrumb: ['首页']
      },
      component: () => import(/* webpackChunkName: "home" */ '@/pages/Home.vue')
    },
    {
      path: '/users',
      name: 'users',
      meta: {
        breadcrumb: ['用户']
      },
      component: () => import(/* webpackChunkName: "users" */ '@/pages/Users.vue')
    },
    {
      path: '/articles',
      name: 'articles',
      meta: {
        breadcrumb: ['文章列表']
      },
      component: () => import(/* webpackChunkName: "articles" */ '@/pages/Articles.vue')
    },
    {
      path: '/article',
      name: 'article',
      meta: {
        breadcrumb: ['文章列表', '文章详情']
      },
      component: () => import(/* webpackChunkName: "article" */ '@/pages/Article.vue')
    },
    {
      path: '/roleactions',
      name: 'roleactions',
      meta: {
        breadcrumb: ['角色管理']
      },
      component: () => import(/* webpackChunkName: "actions" */ '@/pages/RoleActions.vue')
    }
  ]
}

const router = new Router(config)

export default router
