import axios from 'axios'

const api = axios.create({
  baseURL: '/api',
  withCredentials: true
  // timeout: 500
})

// 注意interceptor设置, response是否满足结构 {data, status, message}
api.interceptors.response.use(response => {
  if (response.status === 200) {
    // success: status === 0, else fail
    if (response.data && response.data.code === 200) {
      return Promise.resolve(response.data.data)
    } else {
      return Promise.reject(response.data)
    }
  } else {
    return Promise.reject(response)
  }
}, error => {
  return Promise.reject(error)
})

export default api
