const getType = obj => {
  return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase()
}

const clone = obj => {
  let type = getType(obj)
  let res = type === 'array' ? [] : {}

  if (type === 'array') {
    res = obj.slice(0)
  }
  for (let key in obj) {
    let type = getType(obj[key])
    res[key] = type === 'object' || type === 'array' ? clone(obj[key]) : obj[key]
  }
  return res
}

const formatTime = (time) => {
  if (time) {
    let date = new Date(time)
    let dateString = date.toISOString().replace(/T.+/, '')
    let timeString = date.toTimeString().replace(/\s.+/, '')
    return `${dateString} ${timeString}`
  }
  return time
}

export {
  formatTime,
  getType,
  clone
}