import Router = require('koa-router')
import { auth, authorize } from './middleware/auth'

/**
 * 
 * @param router Router
 * @param app 
 */
export = (router: Router, app:any) => {
  // home
  router.get('/', app.controllers.home.index)
  router.get('/login', auth(), app.controllers.home.login)
  router.get('/page/:id', app.controllers.page.index)
  router.get('/admin(/.*)?', auth(), authorize(), app.controllers.admin.index)
  // router.get('(/.*)?', app.controllers.home.notFound)
}