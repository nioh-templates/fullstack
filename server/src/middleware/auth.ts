import { Context } from 'koa'
import { getManager } from 'typeorm'
import { User } from '../entity/User'
import { decrypt } from '../utility/crypto'


export const auth = () => {
  return async (ctx: Context, next: () => Promise<any>) => {
    let valid = false

    const token = ctx.cookies.get('AUTH_SSO')
    if (token) {
      // Decipher
      try {
        const tokendata = JSON.parse(decrypt(token))
        const Repo = getManager().getRepository(User)
        const user = await Repo.findOne({ username: tokendata.username })
        console.log(tokendata, user)
        if (user) {
          ctx.state.user = user
          valid = true
        }
      } catch (err) {
        console.log('catch error', err)
      }
      // const user 
    }

    // if (!valid) {
    //   return ctx.body = {
    //     code: -1,
    //     message: 'no'
    //   }
    // }

    return next()
  }
}

export const authorize = (type = 'page') => {
  return async (ctx: Context, next: () => Promise<any>) => {
    if (ctx.state.user) {
      return next()
    } else {
      if (type === 'page') {
        return ctx.redirect('/login')
      } else {
        return ctx.body = {
          code: -1,
          message: 'invalid user'
        }
      }
    }
  }
}