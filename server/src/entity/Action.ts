import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Unique } from 'typeorm'
@Entity()
@Unique('idx-action-id', ['action_id'])
export class Action {

  @PrimaryGeneratedColumn({
    type: 'bigint',
    comment: 'id'
  })
  id: number

  @CreateDateColumn({
    comment: '创建时间'
  })
  gmt_create: Date

  @CreateDateColumn({
    comment: '更新时间'
  })
  gmt_modified: Date

  @Column({
    comment: '操作Key'
  })
  action_key: string

  @Column({
    comment: '操作Value'
  })
  action_value: string
}