import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Unique } from 'typeorm'

@Entity()
@Unique('idx-role-id', ['role_id'])
export class Role {
  
  @PrimaryGeneratedColumn({
    type: 'bigint',
    comment: 'id'
  })
  id: number

  @CreateDateColumn({
    comment: '创建时间'
  })
  gmt_create: Date

  @CreateDateColumn({
    comment: '更新时间'
  })
  gmt_modified: Date

  @Column({
    comment: '角色key'
  })
  role_key: string

  @Column({
    comment: '角色名字'
  })
  role_value: string
}