import { Length } from 'class-validator'
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Generated, Unique } from 'typeorm'

@Entity()
@Unique('idx-user-name', ['username'])
export class User {

  @PrimaryGeneratedColumn({
    type: 'bigint',
    comment: 'id'
  })
  id: number

  @CreateDateColumn({
    comment: '创建时间'
  })
  gmt_create: Date

  @CreateDateColumn({
    comment: '更新时间'
  })
  gmt_modified: Date

  @Column({
    comment: 'user id'
  })
  @Generated('uuid')
  uuid: string

  @Column({
    comment: 'username'
  })
  @Length(1, 30)
  username: string

  @Column({
    comment: 'password',
  })
  password: string

  @Column({
    nullable: true,
    comment: 'email'
  })
  email: string

  @Column({
    nullable: true,
    comment: 'phone'
  })
  phone: string

  @Column({
    nullable: true,
    comment: '昵称'
  })
  nickname: string

  @Column({
    comment: '是否激活',
    default: true
  })
  active: boolean
}