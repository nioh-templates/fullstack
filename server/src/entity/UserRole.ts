import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column } from 'typeorm'

@Entity()
export class User_Role {

  @PrimaryGeneratedColumn({
    type: 'bigint',
    comment: 'id'
  })
  id: number

  @CreateDateColumn({
    comment: '创建时间'
  })
  gmt_create: Date

  @CreateDateColumn({
    comment: '更新时间'
  })
  gmt_modified: Date

  @Column({
    comment: 'user id'
  })
  user_id: string

  @Column({
    comment: 'role key'
  })
  role_key: string

}