import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column } from 'typeorm'

@Entity()
export class Role_Action {

  @PrimaryGeneratedColumn({
    type: 'bigint',
    comment: 'id'
  })
  id: number

  @CreateDateColumn({
    comment: '创建时间'
  })
  gmt_create: Date

  @CreateDateColumn({
    comment: '更新时间'
  })
  gmt_modified: Date

  @Column({
    comment: 'role id'
  })
  role_key: string

  @Column({
    comment: 'action id'
  })
  action_key: string

}