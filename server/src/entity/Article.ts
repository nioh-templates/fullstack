import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Generated} from 'typeorm'

@Entity()
export class Article {

  @PrimaryGeneratedColumn({
    type: 'bigint',
    comment: 'id'
  })
  id: number

  @CreateDateColumn({
    comment: '创建时间'
  })
  gmt_create: Date

  @CreateDateColumn({
    comment: '更新时间'
  })
  gmt_modified: Date

  @Column({
    comment: '唯一ID'
  })
  @Generated('uuid')
  uuid: string

  @Column({
    comment: 'title'
  })
  title: string

  @Column({
    nullable: true,
    comment: '描述'
  })
  description: string

  @Column({
    nullable: true,
    comment: '关键词'
  })
  keywords: string

  @Column({
    nullable: true,
    comment: '缩略图'
  })
  pic: string

  @Column({
    comment: '内容',
    type: 'mediumtext'
  })
  content: string

  @Column({
    comment: '创建者'
  })
  creator: string

  @Column({
    nullable: true,
    comment: '更新者'
  })
  updator: string

  @Column({
    comment: '启用',
    default: true
  })
  active: boolean

  @Column({
    comment: '状态',
    default: 1
  })
  status: number

}