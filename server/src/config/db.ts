import * as env from '@nioh/env'
import { ConnectionOptions } from 'typeorm'
import { Article } from '../entity/Article'
import { User } from '../entity/User'

const database = {
  local: {
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '1qaz@WSX',
    database: 'demo',
    synchronize: true
  },
  dev: {
    host: '',
    port: 3306,
    username: '',
    password: '',
    database: 'demo',
    synchronize: false
  },
  qa: {
    host: '',
    port: 3306,
    username: '',
    password: '',
    database: 'demo',
    synchronize: false
  },
  release: {
    host: '',
    port: 3306,
    username: '',
    password: '',
    database: 'demo',
    synchronize: false
  }
}

const type = env.LOCAL ? 'local' : env.DEV ? 'dev' : env.QA ? 'qa' : 'release'
const DB = database[type]

const config:ConnectionOptions = {
  type: 'mysql',
  host: DB.host,
  port: DB.port,
  username: DB.username,
  password: DB.password,
  database: DB.database,
  synchronize: DB.synchronize,
  logging: true,
  entities: [
    Article,
    User
  ]
}

export default config
