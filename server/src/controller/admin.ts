import { Context } from 'koa'

export const index = async (ctx: Context) => {

  // get browser resource info, which in /static
  const info = ctx.helper.getStaticInfo('admin')

  // get rendered string
  const html = await ctx.render('admin', {
    ...info,
    state: ctx.state
  }, false)

  ctx.body = html
}