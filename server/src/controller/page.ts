import { getManager } from 'typeorm'
import { Article } from '../entity/Article'

export const index = async ctx => {

  // const info = ctx.helper.getStaticInfo('home')
  const id = ctx.params.id

  const Repo = getManager().getRepository(Article)
  const article = await Repo.findOne({uuid: id})

  const html = await ctx.render('page', {
    article
    // ...info
  }, false)

  ctx.body = html
}