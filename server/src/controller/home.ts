import { Context } from 'koa'

export const index = async ctx => {

  const info = ctx.helper.getStaticInfo('home')

  const html = await ctx.render('home', {
    ...info
  }, false)

  ctx.body = html
}

export const login = async ctx => {
  // 已登录
  if (ctx.state.user) {
    ctx.redirect('/')
  } else {
    // 未登录
    const info = ctx.helper.getStaticInfo('home')
    console.log(info)
    const html = await ctx.render('login', {
      ...info
    }, false)
  
    ctx.body = html
  }
}