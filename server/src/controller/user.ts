import { Context } from 'koa';
import { Controller, Ctx, Get, Post, Put, Redirect, Res, UseBefore } from 'routing-controllers'
import { Brackets, getManager } from 'typeorm';
import { User } from '../entity/User';
import * as crypto from 'crypto'
import { validate } from 'class-validator';
import { encrypt } from '../utility/crypto';
import { auth, authorize } from '../middleware/auth';

const handler = async (fn: () => Promise<any>)=> {
  let res:any
  try {
    const data = await fn()
    res = {
      code: 0,
      message: 'success',
      data
    }
  } catch (err) {
    res = {
      code: -1,
      message: 'error',
      data: err
    }
  }
  return res
}

const getRootDomain = domain => {
  if (/[a-z]/i.test(domain)) {
    domain = domain.replace(/.+\.([^.]+\.[^.]+)$/, '$1')
  }
  return domain
}


@Controller()
export class UserController {

  @Post('/user')
  @UseBefore(auth(), authorize('json'))
  async create(@Ctx() ctx: Context) {
    ctx.type = 'json'
    // validate body
    const { username, password } = ctx.request.body || {}
    console.log(password, password.length)
    if (password.length < 8 || password.length > 20) {
      return {
        code: -1,
        message: 'password too short'
      }
    }
    const hash = crypto.createHash('md5')
    const SALT = 'ADMIN'
    hash.update(password + SALT)
    console.log(hash)

    const Repo = getManager().getRepository(User)
    const user = Repo.create({ username, password: hash.digest('hex') })

    let errors = await validate(user)

    if (errors.length > 0) {
      return errors
    } else {
      const res = await handler(async () => {
        const data = await Repo.save(user)
        return data
      })

      return res
    }
  }

  /**
   * 登录
   * @param ctx
   */
  @Post('/user/login')
  async login(@Ctx() ctx: Context, @Res() response: any) {
    ctx.type = 'json'
    const { username, password } = ctx.request.body || {}
    const Repo = getManager().getRepository(User)
    const user = await Repo.findOne({username})

    const hash = crypto.createHash('md5')
    const SALT = 'ADMIN'
    hash.update(password + SALT)

    if (hash.digest('hex') === user.password) {
      // match
      const info = {
        username,
        last: Date.now()
      }
      const token = encrypt(JSON.stringify(info))
      ctx.cookies.set('AUTH_SSO', token, {
        domain: getRootDomain(ctx.hostname),
        maxAge: 1000 * 60 * 60 * 24 * 7,
        httpOnly: true,
        signed: false
      })
      return {
        code: 0,
        message: 'success'
      }
    } else {
      return {
        code: -1,
        message: 'invalid'
      }
    }

  }

  @Get('/user/logout')
  async logout(@Ctx() ctx: Context, @Res() response: any) {
    ctx.cookies.set('AUTH_SSO', '', {
      domain: getRootDomain(ctx.hostname),
      signed: false
    })
    response.redirect(ctx.query.redirect || '/')
    return response
  }

  /**
   * 
   * @param ctx
   */
  @Put('/user')
  @UseBefore(auth(), authorize('json'))
  async update(@Ctx() ctx: Context) {
    ctx.type = 'json'

    const { password, active } = ctx.request.body || {}

    const Repo = getManager().getRepository(User)
    const user = await Repo.findOne({username: String(ctx.query.username)})

    let res:any
    if (user) {
      res = await handler(async () => {
        Object.assign(user, { password, active })
        const data = await Repo.save(user)
        return data
      })
    } else {
      res = {
        code: -2,
        message: 'Error: Invalid user id'
      }
    }

    return res
  }


  @Get('/user')
  @UseBefore(auth(), authorize('json'))
  async read(@Ctx() ctx: Context) {
    ctx.type = 'json'

    let res:any

    const { username } = ctx.query
    const take = Number(ctx.query.size) || 10
    const skip = Number(ctx.query.index) * take || 0

    try {
      res = await getManager()
        .createQueryBuilder(User, 'user')
        .where(new Brackets(qb => {
          // qb.where('user.active = :active', {active: false})
          if (username) qb.andWhere('user.username = :id', { username: username })
          // if (alias) qb.andWhere('user.alias LIKE :alias', { alias: `%${alias}%` })
          // if (title) qb.andWhere('user.title LIKE :title', { title: `%${title}%` })
        }))
        .orderBy('user.id', 'DESC')
        .skip(skip)
        .take(take)
        .getManyAndCount()
    } catch (err) {
      console.log('error', err)
      res = [[], 0]
    }

    let [list, total] = res

    return {
      code: 0,
      message: 'ok',
      data: {
        list,
        total
      }
    }
  }

}