import { Context } from 'koa'
import { Controller, Ctx, Get, Post, Put, UseBefore } from 'routing-controllers'
import { Brackets, getManager } from 'typeorm'
import { Article } from '../entity/Article';
import { auth, authorize } from '../middleware/auth';

const handler = async (fn: () => Promise<any>)=> {
  let res:any
  try {
    const data = await fn()
    res = {
      code: 0,
      message: 'success',
      data
    }
  } catch (err) {
    res = {
      code: -1,
      message: 'error',
      data: err
    }
  }
  return res
}


@Controller()
@UseBefore(auth(), authorize('json'))
// @UseBefore()
export class ArticleController {

  @Post('/article')
  async create(@Ctx() ctx: Context) {
    ctx.type = 'json'
    // validate body
    const body = ctx.request.body || {}
    body.creator = 'demo user'

    const Repo = getManager().getRepository(Article)
    const article = Repo.create(body)

    const res = await handler(async () => {
      const data = await Repo.save(article)
      return data
    })

    return res

  }

  @Put('/article')
  async update(@Ctx() ctx: Context) {
    ctx.type = 'json'

    const body = ctx.request.body || {}

    const Repo = getManager().getRepository(Article)
    const article = await Repo.findOne({uuid: String(ctx.query.id)})

    let res:any
    if (article) {
      res = await handler(async () => {
        delete body.id
        Object.assign(article, body)
        article.updator = 'bbb'
        const data = await Repo.save(article)
        return data
      })
    } else {
      res = {
        code: -2,
        message: 'Error: Invalid article id'
      }
    }

    return res
  }


  @Get('/article')
  async read(@Ctx() ctx: Context) {
    ctx.type = 'json'

    let res:any

    const { id } = ctx.query
    const take = Number(ctx.query.size) || 10
    const skip = Number(ctx.query.index) * take || 0

    try {
      res = await getManager()
        .createQueryBuilder(Article, 'article')
        .where(new Brackets(qb => {
          qb.where('article.status != 0')
          if (id) qb.andWhere('article.uuid = :id', { id: id })
          // if (alias) qb.andWhere('article.alias LIKE :alias', { alias: `%${alias}%` })
          // if (title) qb.andWhere('article.title LIKE :title', { title: `%${title}%` })
        }))
        .orderBy('article.id', 'DESC')
        .skip(skip)
        .take(take)
        .getManyAndCount()
    } catch (err) {
      console.log('error', err)
      res = [[], 0]
    }

    let [list, total] = res

    return {
      code: 0,
      message: 'ok',
      data: {
        list,
        total
      }
    }
  }

}