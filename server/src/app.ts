import 'reflect-metadata'
import { createConnection } from 'typeorm'
import App from '@nioh/core'
import { useKoaServer } from 'routing-controllers'

import ormConfig from './config/db'
import { ArticleController } from './controller/article'
import { UserController } from './controller/user'

createConnection(ormConfig).then(async connection => {
  const app = new App({
    baseDir: __dirname,
    port: 3000,
    keys: 'demo'
  })

  // app
  //   .use(app.middlewares.cdn.index)

  useKoaServer(app, {
    routePrefix: '/api',
    defaultErrorHandler: false,
    middlewares: [],
    controllers: [
      ArticleController,
      UserController
    ]
  })

  app.start()
}).catch(error => {
  console.error('TypeORM connection erroe', error)
})
