import * as crypto from 'crypto'

const algorithm = 'aes-128-cbc'
const password = 'fedcba9876543210'
// 改为使用异步的 `crypto.scrypt()`。
// const key = crypto.scryptSync(password, 'salt_value', 24)
// console.log('key', key)
const key = password
// IV 通常与密文一起传递。

export const encrypt = (value) => {
  // 初始化向量
  const iv = Buffer.alloc(16, 0)
  const cipher = crypto.createCipheriv(algorithm, key, iv)

  let encrypted = cipher.update(value, 'utf8', 'hex')
  encrypted += cipher.final('hex')
  console.log(encrypted)
  // 使用相同的算法、密钥和 iv 进行加密。
  return encrypted
}

export const decrypt = value => {
  const iv = Buffer.alloc(16, 0)
  const decipher = crypto.createDecipheriv(algorithm, key, iv)
  let decrypted = decipher.update(value, 'hex', 'utf8')
  decrypted += decipher.final('utf8')
  console.log(decrypted)
  return decrypted
}